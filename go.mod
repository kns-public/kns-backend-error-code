module gitlab.com/kns-public/kns-backend-error-code

go 1.20

require (
	github.com/dave/jennifer v1.7.0
	github.com/iancoleman/strcase v0.3.0
)
