package main

import (
	"encoding/json"
	"fmt"
	"html"
	"os"
	"strings"

	"github.com/dave/jennifer/jen"
	"github.com/iancoleman/strcase"
)

func GenerateGoCode() {
	fileName := "codes.json"
	file, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	data := make(map[string]map[string]string)

	// Unmarshal the JSON into the map
	if err := json.Unmarshal(file, &data); err != nil {
		fmt.Println("Error parsing JSON:", err)
		return
	}

	f := jen.NewFile("codes")

	for key, value := range data {
		constantName := strcase.ToScreamingSnake(value["short"])
		f.Const().Id(constantName).Op("=").Lit(key)
	}

	// Generate the Go code file
	err = f.Save("codes/codes.go")
	if err != nil {
		fmt.Println("Error saving Go code file:", err)
		return
	}

	fmt.Println("Go code file generated successfully: codes.go")
}

func GenerateTsCode() {
	fileName := "codes.json"
	file, err := os.ReadFile(fileName)
	if err != nil {
		fmt.Println("Error reading file:", err)
		return
	}

	data := make(map[string]map[string]string)

	if err := json.Unmarshal(file, &data); err != nil {
		fmt.Println("Error parsing JSON:", err)
		return
	}

	var tsCode strings.Builder
	tsCode.WriteString("// Generated TypeScript code\n")
	tsCode.WriteString("type ErrorCodes={[key:string]:ErrorCodeDetail};type ErrorCodeDetail={[key:string]:string};")
	tsCode.WriteString("export const codes:ErrorCodes={")
	for key, value := range data {
		constantName := "'" + key + "'"
		tsCode.WriteString(fmt.Sprintf("%s:{'en': '%s', 'vi': '%s'},", constantName, html.EscapeString(value["en"]), html.EscapeString(value["vi"])))
	}
	tsCode.WriteString("};")
	tsCode.WriteString("const getErrorMessage=(code:string,lang:string=\"vi\")=>{if(codes[code]){return codes[code][lang]||codes[code][\"en\"]}else{return\"Something went wrong (\"+code+\")\"}};")
	tsCode.WriteString("export {getErrorMessage};")
	tsFileName := "codes.ts"
	if err := os.WriteFile(tsFileName, []byte(tsCode.String()), 0644); err != nil {
		fmt.Printf("Error writing TypeScript file: %v\n", err)
		return
	}

	fmt.Printf("TypeScript file generated successfully: %s\n", tsFileName)
}

func main() {
	GenerateGoCode()
	GenerateTsCode()
}
