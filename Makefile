run:
	go run main.go
	
release:
	npx tsc
	npm version patch
	git push
	npm publish